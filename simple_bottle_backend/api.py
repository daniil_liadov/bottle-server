import json
import datetime
import base64
import smtplib
import glob
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

from reportlab.pdfgen import canvas
from bottle import run, response, request
import bottle

from database_utils import db, query_db, execute_query, sql_formatter, bool_to_sql
from useful_utils import  bytes_to_dict, random_string

# smtp server

MY_ADDRESS = os.environ.get('SMTP_EMAIL_ADDRESS', 'smtpsmtp18@gmail.com')
PASSWORD = os.environ.get('SMTP_EMAIL_PASSWORD', 'Community1234')

SMTP_SERVER = smtplib.SMTP(os.environ.get('SMTP_TYPE', 'smtp.gmail.com:587'))
# SMTP_SERVER.starttls()
# SMTP_SERVER.login(MY_ADDRESS, PASSWORD)

app = bottle.app()

# CORS headers over the api app


@bottle.error(405)
def method_not_allowed(res):
    if request.method == 'OPTIONS':
        new_res = bottle.HTTPResponse()
        new_res.set_header('Access-Control-Allow-Origin', '*')
        new_res.set_header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS')
        new_res.set_header('Access-Control-Allow-Headers', 'origin, content-type, accept')
        return new_res
    res.headers['Allow'] += ', OPTIONS'
    return request.app.default_error_handler(res)


@bottle.hook('after_request')
def enableCORSAfterRequestHook():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

# GET routes

@app.route('/categories/')
def get_categories():
    my_query = query_db('SELECT * '
                        'FROM public."Category"')
    lookup = json.dumps(my_query)
    return lookup


@app.route('/courses/')
def get_courses():
    my_query = query_db('select * '
                        'from public."Course"')
    lookup = json.dumps(my_query)
    return lookup


@app.route('/users/emails')
def get_emails():
    return json.dumps(query_db('select username '
                               'from public."User"'))


@app.route('/orders/<uid>')
def get_orders(uid):
    uid = str(base64.b64decode(uid), 'ascii')
    to_serialize = \
        query_db(f'select order_to_sign_pk,date,order_to_sign_user,order_to_sign_course, is_approved,is_completed,'
                 f'is_paid,username '
                 f'from public."Order_to_sign" inner join public."User" '
                 f'on public."Order_to_sign".order_to_sign_user = public."User".user_pk '
                 f'where user_pk = {uid}')
    for element in to_serialize:
        element.update({'date': element['date'].strftime('%d-%m-%y')})
    return json.dumps(to_serialize)


@app.route('/courses/<identifier>')
def get_course(identifier):
    response_body = query_db(f'select '
                             f'* from public."Course" '
                             f'where course_pk={sql_formatter(identifier)}')[0]
    if response_body is None:
        response.status = 404
        return response
    return json.dumps(response_body)


@app.route('/tags')
def get_tags():
    return json.dumps(query_db('select * '
                               'from public."Tag"'))


@app.route('/tags/<uid>')
def get_courses(uid):
    if uid == '-1':
        return json.dumps(query_db('select * from public."Course"'))
    return json.dumps(query_db(f'select course_pk, name, description, is_available, price, course_category, '
                               f'amount_available, image_url '
                               f'from public."Course_Tag" '
                               f'inner join public."Course" '
                               f'on public."Course_Tag".course_fk = public."Course".course_pk where tag_fk ={uid}'))


@app.route('/orders/all/<identifier>')
def get_orders(identifier):
    uid = str(base64.b64decode(identifier), 'ascii')
    user = query_db(f'select user_pk from public."User" where user_pk={uid}')[0]
    if user is None:
        response.status = 403
        return response
    query = query_db('select order_to_sign_pk,date,order_to_sign_user,order_to_sign_course,'
                     'is_approved,is_completed,is_paid,username '
                     'from public."Order_to_sign" '
                     'inner join public."User" '
                     'on public."Order_to_sign".order_to_sign_user = public."User".user_pk ')
    for element in query:
        element.update({'date': element['date'].strftime('%d-%m-%y')})
    return json.dumps(query)
# POST routes


@app.post('/order/new/')
def create_order():
    body = bytes_to_dict(request.body)
    course_id = body['course_id']
    user_id = body['user_id']
    is_paid = bool_to_sql(body['is_paid'])
    is_completed = 'FALSE'
    orders = query_db('select order_to_sign_course, order_to_sign_user, is_completed '
                      'from public."Order_to_sign"')
    for element in orders:
        if element['order_to_sign_course'] == course_id and element['order_to_sign_user'] == user_id and\
                element.get('is_completed') is False:
            response.status = 403
            return response
    date = sql_formatter(datetime.datetime.now().date().strftime('%d-%m-%y'))
    amount = query_db(f'select amount_available '
                      f'from public."Course" '
                      f'where course_pk={course_id}')[0]
    if amount == 0:
        response.status = 403
        return response
    execute_query(f'insert into public."Order_to_sign" (order_to_sign_course, order_to_sign_user, date, is_completed, '
                  f'is_paid, is_approved)'
                  f'values({course_id}, {user_id}, {date}, {is_completed}, {is_paid}, '
                  f'{is_completed})')
    execute_query(f'update public."Course" '
                  f'set amount_available = amount_available - 1 '
                  f'where course_pk={course_id}')
    msg = MIMEMultipart()
    course_name = query_db(f'select name '
                           f'from public."Course" '
                           f'where course_pk={course_id}')[0]['name']
    user_email = query_db(f'select username '
                          f'from public."User" '
                          f'where user_pk={user_id}')[0]['username']
    text = f'You have successfully ordered sign on course "{course_name}"! \n You just need to wait for admin\'s ' \
           f'approvement. \n You will receive a .pdf document when admin accepts your order.'
    msg.attach(MIMEText(text, "plain"))
    SMTP_SERVER.sendmail(MY_ADDRESS, user_email, msg.as_string())
    response.status = 201
    return response


@app.post('/users/update_balance/')
def update_balance():
    body = bytes_to_dict(request.body)
    balance = body['balance']
    pk = body['user_id']
    cur = db().cursor()
    cur.execute(f'UPDATE public."User" '
                f'SET account_balance = {balance} '
                f'WHERE user_pk={pk};')
    cur.connection.commit()
    cur.connection.close()
    return response


@app.post('/users/me/')
def get_user():
    token = bytes_to_dict(request.body)['token']
    return json.dumps(query_db('select * '
                               'from public."User" '
                               'where acces_token={} '.format(sql_formatter(token)))[0])


@app.post('/get_auth_token/')
def get_token():
    check_dict = bytes_to_dict(request.body)
    username = sql_formatter(check_dict['email'])
    password = sql_formatter(check_dict['password'])
    token = query_db(f'select acces_token '
                     f'from public."User" '
                     f'where username={username} and '
                     f' password={password}')
    try:
        token = token[0]
    except IndexError:
        response.status = 404
        return response.status
    token_value = token['acces_token']
    user = query_db('select * '
                    'from public."User" '
                    'where acces_token={} '.format(sql_formatter(token_value)))[0]
    to_dump = dict(token=token_value, user=user)
    return json.dumps(to_dump)


@app.delete('/orders/delete_order/<uid>')
def delete_order(uid):
    uid = str(base64.b64decode(uid), 'ascii')
    try:
        course_id = query_db(f'select order_to_sign_course '
                             f'from public."Order_to_sign" '
                             f'where order_to_sign_pk={uid}')[0]['order_to_sign_course']
    except IndexError:
        response.status = 403
        return response
    execute_query(f'DELETE FROM public."Order_to_sign" '
                  f'WHERE order_to_sign_pk={uid}')
    execute_query(f'update public."Course" '
                  f'set amount_available = amount_available+1 '
                  f'where course_pk={course_id}')
    return response


@app.put('/orders/update_order/<uid>')
def update_order(uid):
    order = bytes_to_dict(request.body)
    uid = str(base64.b64decode(uid), 'ascii')
    is_approved = order['is_approved']
    is_paid = bool_to_sql(order['is_paid'])
    is_completed = bool_to_sql(order['is_completed'])
    username = sql_formatter(order['username'])
    execute_query(f'update public."Order_to_sign"'
                  f' set is_approved = {bool_to_sql(is_approved)}, is_paid = {is_paid},'
                  f' is_completed ={is_completed} where order_to_sign_pk={uid}')
    user = query_db(f'select username, user_pk from public."User" '
                    f'where username = {username}')[0]
    user_pk = user['user_pk']
    order = query_db(f'select * '
                     f'from public."Order_to_sign" '
                     f'where order_to_sign_user = {user_pk}')[0]
    order_course = order['order_to_sign_course']
    order_pk = order['order_to_sign_pk']
    course = query_db(f'select * '
                      f'from public."Course" '
                      f'where course_pk={order_course}')[0]
    email = user['username']
    order_date = 'User has signed on course on ' + order['date'].strftime('%d-%m-%y')
    unique_name = f'./docs/{email}_#{order_pk}-order_course-{order_course}_uid-{random_string(10)}.pdf'
    for element in glob.glob("./docs/*.pdf"):
        if email in element and f'order_course{str(order_course)}' in element:
            response.status = 403
            return response
    if is_approved and order['is_paid']:
        c = canvas.Canvas(unique_name)
        c.drawString(10, 810, 'This is unique pdf that confirms approvement of administration of site. '
                     'Keep it in big secret!')
        c.drawString(10, 790, 'User has signed on course :'+course['name'])
        c.drawString(10, 770, 'Amount of UAH to be paid:'+str(course['price']))
        c.drawString(10, 750, order_date)
        was_approved = f'Was approved on {datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")} by admin.'
        c.drawString(10, 730, was_approved)
        c.save()
        msg = MIMEMultipart()
        msg.attach(MIMEText('Congratulations!\n Administration have approved your order and you received your '
                   'own unique pdf file.\n Keep it in secret because it will be useful for next steps!', "plain"))
        msg.attach(MIMEApplication(open(unique_name).read(), 'pdf'))
        msg['Subject'] = "Your subscription has been approved!"
        SMTP_SERVER.sendmail(MY_ADDRESS, email, msg.as_string())
    else:
        response.status = 403
        return response
    response.status = 204
    return response


@app.post('/users/new_user/')
def register():
    parsed_dict = bytes_to_dict(request.body)
    username = sql_formatter(parsed_dict['username'])
    password = sql_formatter(parsed_dict['password'])
    acces_token = sql_formatter(random_string(20))
    is_superuser = 'FALSE'
    account_balance = 0
    phone_number = sql_formatter(parsed_dict['phone_number'])
    execute_query(f'insert into public."User" (username, password, acces_token, phone_number, account_balance, '
                  f'is_superuser) '
                  f'values ({username},{password},{acces_token},{phone_number},{account_balance},{is_superuser})')
    return response


@app.get('/emergency/')
def emergency():
    query = query_db('Select sum(price), username '
                     'from public."Order_to_sign" '
                     'inner join public."User" '
                     'on public."Order_to_sign".order_to_sign_user = public."User".user_pk '
                     'inner join public."Course" '
                     'on public."Order_to_sign".order_to_sign_course = public."Course".course_pk '
                     'where is_paid = true group by username')
    seq = [x['sum'] for x in query]
    for element in query:
        if element['sum'] == max(seq):
            return json.dumps(element)


run(host='localhost', port=8080)

