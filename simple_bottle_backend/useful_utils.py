import json
import random
import string


def bytes_to_dict(to_read):
    s = to_read.read().decode('utf-8')
    json_acceptable_string = s.replace("'", "\"")
    d = json.loads(json_acceptable_string)
    return d


def random_string(number):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(number))
