import psycopg2

# Database utilities


def db(database_name='tkp'):
    return psycopg2.connect(dbname=database_name, user="postgres", password="pass")


def query_db(query, args=(), one=False):
    cur = db().cursor()
    cur.execute(query, args)
    r = [dict((cur.description[i][0], value) for i, value in enumerate(row)) for row in cur.fetchall()]
    cur.connection.close()
    return (r[0] if r else None) if one else r


def execute_query(query):
    cur = db().cursor()
    cur.execute(query)
    cur.connection.commit()
    cur.connection.close()


def sql_formatter(to_format):
    return '\'{}\''.format(to_format)


def bool_to_sql(to_format):
    if to_format is True:
        return 'TRUE'
    return 'FALSE'
